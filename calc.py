
# Code by Jeisson Leon - creative licensed Lion Team inc


from tracemalloc import stop


print("-----" * 12, "\n" "\n" "             Bienvenido Al Sistema de BCI" "\n")
print("-----" * 12)

print("Cual es tu nombre?:")
name_User = input("Cual es tu nombre?: ")

print("-----" * 12)

print(f'Hola {name_User.capitalize()}, como estas? \n')
weight = float(input(" --> Cuantos kilogramos pesas?: "))
height = float(input(" --> Cuantos metros de altura tienes?: "))

print(" ")
print("-----" * 12)

ans = weight / (height ** 2)

if ( ans == 0 ):
    print("!!! - Que?... Estas muerto o mientes")
    exit()
elif ( ans <= 16 ):
    ind_ans = "tienes Delgades Severa"
elif ( ans >= 16 and ans <= 16.99 ):
    ind_ans = "tienes Delgadez Moderada"
elif ( ans >= 17 and ans <= 18.49 ):
    ind_ans = "tienes Delgadez Aceptable"
elif ( ans >= 18.5 and ans <= 24.99 ):
    ind_ans = "tienes el Peso Normal"
elif ( ans >= 25 and ans <= 34.99 ):
    ind_ans = "tienes Sobrepeso"
elif ( ans >= 30 and ans <= 34.99 ):
    ind_ans = "tienes Obesidad de tipo 1"
elif ( ans >= 35 and ans <= 40 ):
    ind_ans = "tienes Obesidad de tipo 2"
elif ( ans >= 40 and ans <= 49.99 ):
    ind_ans = "tienes Obesidad de tipo 3 - OBESIDAD MORBIDA"
elif ( ans <= 50 ):
    ind_ans = "tienes Obesidad de tipo 4 - EXTREMA"
else:
    print("Ṕon algo valido we")
    exit()

print(f' {name_User.capitalize()} tu IMC es {round(ans, 2)} y {ind_ans}')
print("-----" * 12)
